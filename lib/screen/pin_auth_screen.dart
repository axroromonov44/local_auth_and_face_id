import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:local_auth_and_face_id/auth_service/local_auth_service.dart';
import 'package:local_auth_and_face_id/widget/custom_keyboard.dart';
import 'package:local_auth_and_face_id/widget/custom_pincodefield.dart';

class PinAuthScreen extends StatefulWidget {
  const PinAuthScreen({Key? key}) : super(key: key);

  @override
  State<PinAuthScreen> createState() => _PinAuthScreenState();
}

class _PinAuthScreenState extends State<PinAuthScreen> {
  String pin = "";
  bool authenticated = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff3CBA92),
      body: Column(
        children: [
          const SizedBox(
            height: 200,
          ),
          const Text(
            "PIN KOD",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          const Text(
            "Hozirgi PIN-kodni kiriting",
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.normal,
            ),
          ),
          const SizedBox(
            height: 100,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              for (int i = 0; i < 4; i++)
                CustomPinCodeField(
                  key: Key('pinField$i'),
                  pin: pin,
                  pinCodeFieldIndex: i,
                ),
            ],
          ),
          const SizedBox(
            height: 50,
          ),
          CustomKeyBoard(
            onChanged: (v) {
              if (kDebugMode) {
                print(v);
                pin = v;
                setState(() {});
              }
            },
            specialKey: const Icon(
              Icons.fingerprint,
              key: Key('fingerprint'),
              color: Color(0xff007A4E),
              size: 50,
            ),
            specialKeyOnTap: () async {
              final authenticate = await LocalAuth.authenticate();
              setState(() {
                authenticated = authenticate;
              });
            },
            maxLength: 4,
          ),
        ],
      ),
    );
  }
}
