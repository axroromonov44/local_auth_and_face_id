import 'package:flutter/material.dart';
import 'package:local_auth_and_face_id/screen/pin_auth_screen.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Example Pin Screen',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const PinAuthScreen(),
    );
  }
}

