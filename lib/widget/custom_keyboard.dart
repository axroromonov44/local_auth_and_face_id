import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomKeyBoard extends StatefulWidget {
  final Widget? specialKey;
  final Function(String)? onChanged;
  final Function(String)? onCompleted;
  final Function()? specialKeyOnTap;
  final int maxLength;

  const CustomKeyBoard({
    Key? key,
    required this.maxLength,
    this.specialKey,
    this.onChanged,
    this.specialKeyOnTap,
    this.onCompleted,
  })  : assert(maxLength > 0),
        super(key: key);
  @override
  _CustomKeyBoardState createState() => _CustomKeyBoardState();
}

class _CustomKeyBoardState extends State<CustomKeyBoard> {
  String value = "";
  Widget buildNumberButton({int? number, Widget? icon, Function()? onPressed}) {
    getChild() {
      if (icon != null) {
        return icon;
      } else {
        return Container(
          height: 56,
          width: 56,
          alignment: Alignment.center,
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Color(0xffDEECDD),
              borderRadius: BorderRadius.circular(28)
          ),
          child: Text(
            number?.toString() ?? "",
            style: const TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w600,
              color: Colors.black,
            ),
          ),
        );
      }
    }

    return Expanded(
        child: CupertinoButton(
            key: icon?.key ?? Key("btn$number"),
            onPressed: onPressed,
            child: getChild()));
  }

  Widget buildNumberRow(List<int> numbers) {
    List<Widget> buttonList = numbers
        .map((buttonNumber) => buildNumberButton(
      number: buttonNumber,
      onPressed: () {
        if (value.length < widget.maxLength) {
          setState(() {
            value = value + buttonNumber.toString();
          });
        }
        widget.onChanged!(value);
        if (value.length >= widget.maxLength &&
            widget.onCompleted != null) {
          widget.onCompleted!(value);
        }
      },
    ))
        .toList();
    return Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: buttonList,
        ));
  }

  Widget buildSpecialRow() {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          buildNumberButton(
            icon: widget.specialKey ??
                const Icon(
                  Icons.circle,
                  key: Key('specialKey'),
                  color: Colors.white,
                  size: 7,
                ),
            onPressed: widget.specialKeyOnTap ??
                    () {
                  if (value.length < widget.maxLength) {
                    if (!value.contains(".")) {
                      setState(() {
                        value = "$value.";
                      });
                    }
                  }
                  widget.onChanged!(value);
                  if (value.length >= widget.maxLength &&
                      widget.onCompleted != null) {
                    widget.onCompleted!(value);
                  }
                },
          ),
          buildNumberButton(
            number: 0,
            onPressed: () {
              if (value.length < widget.maxLength) {
                setState(() {
                  value = value + 0.toString();
                });
              }
              widget.onChanged!(value);
              if (value.length >= widget.maxLength &&
                  widget.onCompleted != null) {
                widget.onCompleted!(value);
              }
            },
          ),
          buildNumberButton(
              icon: const Icon(
                Icons.backspace_outlined,
                key: Key('backspace'),
                color: Colors.black,
              ),
              onPressed: () {
                if (value.isNotEmpty) {
                  setState(() {
                    value = value.substring(0, value.length - 1);
                  });
                }
                widget.onChanged!(value);
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(50)
        ),
        child: Column(
          children: [
            buildNumberRow([1, 2, 3]),
            buildNumberRow([4, 5, 6]),
            buildNumberRow([7, 8, 9]),
            buildSpecialRow(),
          ],
        ),
      ),
    );
  }
}
