import 'package:flutter/material.dart';

class CustomPinCodeField extends StatelessWidget {
  final String pin;
  final int pinCodeFieldIndex;

  const CustomPinCodeField({
    Key? key,
    required this.pin,
    required this.pinCodeFieldIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      height: 24,
      width: 24,
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: getFillColorFromIndex,
        borderRadius: BorderRadius.circular(12),
        shape: BoxShape.rectangle,
      ),
      duration: const Duration(microseconds: 40000),
      child: const SizedBox(),
    );
  }

  Color get getFillColorFromIndex {
    if (pin.length > pinCodeFieldIndex) {
      return Color(0xff007A4E);
    } else if (pin.length == pinCodeFieldIndex) {
      return Color(0xffC1DFC4);
    }
    return Color(0xffC1DFC4);
  }
}
